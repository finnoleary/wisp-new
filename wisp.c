#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdarg.h>

typedef struct {
	int indent; /* Used with %noindent for controlling indent() */
	int newline; /* Used with %nonewline for controlling indent() */
	int inheriting;
	int outfd;
	char *inpath, *outpath, *buffer;
	size_t depth;
} State;

typedef struct {
	char *tag;
	int mode, subnode;
	size_t index;
	size_t length;
} Tag;

#include "wisp.h"

struct {
	char *indent, *out_folder;
	int verbose;
} settings = {
	.indent = "  ",
	.out_folder = "./out",
	.verbose = 0,
};

struct {
	size_t current, stacksize;
	Tag *stack;
} tag = {
	.current = 0,
	.stacksize = 0,
	.stack = NULL,
};

enum {
	MODE_NONE,
	MODE_NONEWLINE = 1,
	MODE_NOINDENT  = 2,
	MODE_COMPACT   = 4, /* (a !compact 'foo') => <a>foo</a> */
	MODE_INLINE    = 8  /* (a !inline :foo "bar") => <a foo="bar" /> */
};

#define OPTCMP(value, short, long) \
	(((short) && (strcmp((value), (short))) == 0) || \
	 ((long)  && (strcmp((value), (long))) == 0))

#define eprint(...) fprintf(stderr, __VA_ARGS__)
#define wprint(...) if (settings.verbose) { fprintf(stderr, __VA_ARGS__); }

#define whine0(S, i, fmt) \
	eprint(fmt " at line %lu in %s\n", get_line_count(S->buffer, i), S->inpath)

#define whinen(S, i, fmt, ...) \
	eprint(fmt " at line %lu in %s\n", __VA_ARGS__, get_line_count(S->buffer, i), S->inpath)


int tag_init(size_t size)
{
	if (!(tag.stack = calloc(size, sizeof(Tag)))) {
		perror("calloc");
		return 0;
	}
	tag.stacksize = size;
	return 1;
}


void tag_deinit(void)
{
	if (tag.stack) { free(tag.stack); tag.stack = NULL; }
	tag.stacksize = tag.current = 0;
}


void pushmode(int mode)
{
	if (tag.current == 0) {
		eprint("Unable to push mode! We're at the root?\n");
	}
	tag.stack[tag.current-1].mode = mode;
}


int get_current_mode(void)
{
	if (tag.current <= 0) {
		return 0;
	}
	return tag.stack[tag.current-1].mode;
}


int pushtag(char *s, size_t index, size_t length)
{
	if (tag.current == tag.stacksize) {
		eprint("Exceeded max number of nested tags\n");
		return 0;
	}
	tag.stack[tag.current].tag = s;
	tag.stack[tag.current].index = index;
	tag.stack[tag.current].length = length;
	tag.stack[tag.current].mode = 0;
	tag.stack[tag.current].subnode = 0;
	tag.current++;
	return 1;
}


int poptag(char **s, size_t *index, size_t *length)
{
	if (tag.current == 0) {
		eprint("Can't pop nonexistent tag\n");
		return 0;
	}
	tag.stack[tag.current].mode = 0;
	tag.current--;
	*s = tag.stack[tag.current].tag;
	*index = tag.stack[tag.current].index;
	*length = tag.stack[tag.current].length;

	tag.stack[tag.current].tag = NULL;
	tag.stack[tag.current].index = 0;
	tag.stack[tag.current].length = 0;
	tag.stack[tag.current].mode = 0;
	return 1;

}


void usage(void)
{
	const char *s = "wisp [OPTIONS] <files>\n"
	                "OPTIONS:\n"
			"\t-v, --verbose\n"
			"\t-d, --outdir <dir>\n"
			"\t-i, --indent <string>\n"
			"\t-h, --help\n";
	printf("%s", s);
}


int parse_options(char **argv, int i)
{
	for (; argv[i] && argv[i][0] == '-'; i++)
	{
		if (OPTCMP(argv[i], "-i", "--indent")) {
			wprint("Have option %s\n", argv[i]);
			if (!argv[i+1]) {
				eprint("Missing argument to %s\n", argv[i]);
				return 0;
			}
			i++;
			settings.indent = argv[i];
		}
		else if (OPTCMP(argv[i], "-d", "--outdir")) {
			wprint("Have option %s\n", argv[i]);
			if (!argv[i+1]) {
				eprint("Missing argument to %s\n", argv[i]);
				return 0;
			}
			i++;
			settings.out_folder = argv[i];
			printf("out folder: %s\n", settings.out_folder);
		}
		else if (OPTCMP(argv[i], "-v", "--verbose")) {
			wprint("Have option %s\n", argv[i]);
			settings.verbose = 1;
		}
		else if (OPTCMP(argv[i], "-h", "--help")) {
			usage();
			return 0;
		}
		else {
			eprint("No such argument: %s\n", argv[i]);
			return 0;
		}
	}
	return i;
}


int mkfolder(char *path)
{
	if ((mkdir(path, 0744)) < 0 && errno != EEXIST) {
		perror("mkdir");
		return 0;
	}
	wprint("Output folder set to %s\n", path);
	return 1;
}


#define mkpath(...) mkpath_(__VA_ARGS__, (char*)NULL)
char *mkpath_(char *s, ...)
{
	char *s_orig = s;
	char *s_next = NULL;
	char *d = NULL;
	size_t length = 0;
	size_t offset = 0;
	va_list ap;

	if (!s) {
		eprint("Error creating path.\n");
		return NULL;
	}

	/* find length of path plus how many slashes to add */
	va_start(ap, s);
	while (s) {
		size_t size, slash;
		s_next = va_arg(ap, char *);
		size = strlen(s);
		slash = (s_next && s[size] != '/' && s_next[0] != '/');

		length += size+slash;
		s = s_next;
	}
	va_end(ap);

	s = s_orig;
	if (!(d = calloc(length+1, 1))) {
		perror("calloc");
		eprint("Error creating path\n");
		return NULL;
	}

	/* Join the paths together */
	va_start(ap, s);
	do {
		size_t size, slash;

		s_next = va_arg(ap, char *);
		size = strlen(s);
		/* NB: Possible bug? we do (size-1) not size here */
		slash = (s_next && s[size-1] != '/' && s_next[0] != '/');

		memcpy(d+offset, s, size);
		if (slash) { d[offset+size] = '/'; }
		offset += size+slash;
		s = s_next;
	} while (s);
	va_end(ap);
	d[offset] = '\0';

	return d;
}


#define join(...) _join(__VA_ARGS__, (char*)NULL)
char *_join(char *s, ...)
{
	va_list ap;
	size_t length = 0;
	char *d = s;

	va_start(ap, s);
	while (s) {
		length += strlen(s);
		s = va_arg(ap, char *);
	}
	s = d;
	va_end(ap);
	
	if (!(d = calloc(length + 1, 1))) { return NULL; }

	va_start(ap, s);
	for (length = 0; s;) {
		memcpy(d+length, s, strlen(s));
		length += strlen(s);
		s = va_arg(ap, char *);
	}
	va_end(ap);

	return d;
}



char *simple_filename(char *path)
{
	size_t i, size = 0;
	char *s = NULL;
	if (!path) { eprint("No path given\n"); return NULL; }

	if (!(s = strdup(path))) { perror("strdup"); return NULL; }

	/* find last '/' character in string */
	for (i = strlen(s); i > 0; i--) {
		size++;
		if (s[i] == '/') { i++; break; }
	}
	if (i > 0) {
		/* +1 to include null byte */
		memmove(s, s+i, size-1);
	}

	/* zero the file extension */
	for (i = size; i > 0; i--) {
		if (s[i] == '.') {
			s[i] = '\0';
			break;
		}
	}

	return s;
}


char *load_buffer(const char *path)
{
        int fd = 0;
        char *data = NULL;
        ssize_t len = 0;
        struct stat st = {0};

        if ((access(path, F_OK)) < 0) { return NULL; }

        if ((stat(path, &st)) < 0) { return NULL; }

        if ((fd = open(path, O_RDONLY)) < 0) { goto _throw; }

        if (!(data = calloc(st.st_size+1, 1))) { goto _throw; }

        if ((len = read(fd, data, st.st_size)) < 0) { goto _throw; }

        if (len < st.st_size) { goto _throw; }

        close(fd);

        return data;
_throw:
        if (fd < 0) { close(fd); }
        if (data) { free(data); }
        return NULL;
}


int open_file(char *path)
{
	int fd = -1;
	/*
	if ((access(path, F_OK) < 0) || (access(path, R_OK|W_OK) < 0)) {
		perror(path);
		return -1;
	}
	*/

	if ((fd = open(path, O_CREAT|O_WRONLY|O_TRUNC, 0644)) < 0) {
		perror(path);
		return -1;
	}
	return fd;
}


int setup_state(State *S, char *file, State *inherit)
{
	char *oldfilename = NULL;
	char *filename = NULL;

	if (!file) { return -1; }

	S->indent = 1;
	S->depth = 0;
	S->inpath = file;
	if (!(S->buffer = load_buffer(file))) { goto _throw; }

	S->inheriting = (inherit != NULL);
	if (!inherit) {
		if (!(oldfilename = simple_filename(file))) { goto _throw; }

		if (!(filename = join(oldfilename, ".html"))) { goto _throw; }

		if (!(S->outpath = mkpath(settings.out_folder, filename))) {
			goto _throw;
		}
		if ((S->outfd = open_file(S->outpath)) < 0) {
			goto _throw;
		}
		free(oldfilename);
		free(filename);
	}
	else {
		S->depth = inherit->depth;
		S->outpath = inherit->outpath;
		S->outfd = inherit->outfd;
	}

	return 0;

_throw:
	if (filename)   { free(filename); }
	clear_state(S);
	return -1;
}


void clear_state(State *S)
{
	if (S->inpath)  { S->inpath = NULL; }
	if (S->buffer)  { free(S->buffer);  S->buffer = NULL; }
	if (!S->inheriting) {
		if (S->outpath) { free(S->outpath); }
		if (S->outfd > 0) { close(S->outfd); }
	}
	S->outpath = NULL;
	S->outfd = -1;
	S->depth = 0;
	S->inheriting = 0;
}


size_t get_line_count(char *s, size_t position)
{
	size_t i, lines;
	
	for (lines = 0, i = 0; i < position && s[i]; i++) {
		if (s[i] == '\n') { lines++; }
	}

	return lines;
}



size_t include(State *S, size_t i)
{
	char *s = NULL;
	char *path = NULL;
	size_t length = 0;
	char c;

	if (!S) { eprint("NULL state given to include\n"); return 0; }
	s = S->buffer;

	if (s[i] == '"' || s[i] == '\'') {
		i++;
	}

	for (length = 0; (c = s[i+length]) && !isspace(c) && c != '"' && c != '\''; length++)
		;
	
	if (!length) {
		whine0(S, i, "Unable to open empty file for include");
		return 0;
	}

	if (!(path = calloc(length + 1, 1))) {
		perror("calloc");
		return 0;
	}

	memcpy(path, s+i, length);

	dprintf(S->outfd, "\n");

	wprint("Started parse of included file '%s'\n", path);

	if (!parse_file(path, S)) {
		whinen(S, i, "Error while parsing file for include: %s", path);
		goto _throw;
	}

	free(path);

	return i+length+1;

_throw:
	free(path);
	return 0;
}


size_t directive(State *S, size_t i)
{
	size_t length = 0;
	char c;
	char *s = S->buffer;

	static char verb[] = "verbatim ";
	static char inc[] = "include ";
	static char noind[] = "noindent";
	static char nonew[] = "nonewline";
	const size_t verb_size = sizeof(verb)-1;
	const size_t inc_size = sizeof(inc)-1;
	const size_t noind_size = sizeof(noind)-1;
	const size_t nonew_size = sizeof(nonew)-1;

	if ((strncmp(S->buffer+i, verb, verb_size)) == 0) {
		wprint("Found %s directive\n", verb);
		i += verb_size;
		for (length = 0; (c = s[i+length]) && c != '\n'; length++)
			;
		dprintf(S->outfd, "%.*s", (int)length+1, s+i);
		wprint("Placing '%.*s' in file\n", (int)length+1, s+i);
		i += length;
		return i;
	}
	else if ((strncmp(S->buffer+i, inc, inc_size)) == 0) {
		wprint("Found %s directive\n", inc);
		i += inc_size;
		return include(S, i);
	}
	else if ((strncmp(S->buffer+i, noind, noind_size)) == 0) {
		wprint("Found %s directive\n", noind);
		i += noind_size;
		S->indent = 0;
		return i;
	}
	else if ((strncmp(S->buffer+i, nonew, nonew_size)) == 0) {
		wprint("Found %s directive\n", nonew);
		i += nonew_size;
		S->newline = 0;
		return i;
	}
	else {
		whine0(S, i, "Invalid directive");
		return 0;
	}
}


size_t skip_line(State *S, size_t i)
{
	char c;
	for (; (c = S->buffer[i]) && c != '\n'; i++)
		;
	if (S->buffer[i]) { i++; }
	return i;
}


void indent(State *S, int mode)
{
	if (!S) { eprint("indent was given a null State???\n"); return; }
	size_t depth = S->depth;
	mode = mode >= 0 ? mode : get_current_mode();

	if (mode & (MODE_COMPACT|MODE_INLINE)) { return; }

	if (!S->newline || (mode & MODE_NONEWLINE)) {
		S->newline = 1;
	}
	else {
		dprintf(S->outfd, "\n");
	}

	if (!S->indent || (mode & MODE_NOINDENT)) {
		S->indent = 1;
		return;
	}

	for (; depth > 0; depth--) {
		dprintf(S->outfd, "%s", settings.indent);
	}
}


size_t key(State *S, size_t i)
{
	size_t length = 0;
	char *s = NULL;
	char c;
	if (!S) { eprint("key was given a null State?\n"); return 0; }

	s = S->buffer;

	for (length = 0; (c = s[i+length]) && !isspace(c); length++)
		;
	
	if (!length) { whine0(S, i, "Missing tag key"); return 0; }
	if (!c) { whine0(S, i, "Unexpected EOF"); return 0; }

	wprint("Found key '%.*s'\n", (int)length, s+i);
	dprintf(S->outfd, "%.*s", (int)length, s+i);

	return i+length;
}


size_t value(State *S, size_t i)
{
	size_t length = 0;
	char *s = NULL;
	char c;
	if (!S) { eprint("value was given a null State?\n"); return 0; }

	s = S->buffer;

	for (length = 1; (c = s[i+length]) && c != s[i]; length++) {
		if (c == '\\') {
			length += 2;
		}
	};
	length++;
	
	if (!length) { whine0(S, i, "Missing tag value"); return 0; }
	if (!c) { whine0(S, i, "Unexpected EOF"); return 0; }

	dprintf(S->outfd, "=%.*s", (int)length, s+i);
	wprint("Found value '%.*s'\n", (int)length, s+i);

	return i+length;
}


size_t keys_values(State *S, size_t i)
{
	char *s = NULL;
	char c;
	if (!S) { eprint("keys_values was given a null State?\n"); return 0; }

	s = S->buffer;

	while ((c = s[i]))
	{
		switch (c) {
		case '\0':
			whine0(S, i, "Unexpected EOF");
			break;
		case ' ':
		case '\t':
		case '\n':
			i++;
			break;
		case ':':
			dprintf(S->outfd, " ");
			if (!(i = key(S, i+1))) { return 0; }
			for (; (c = s[i]) && isspace(c); i++)
				;
			if (!c) { whine0(S, i, "Unexpected EOF"); return 0; }
			if (s[i] == '\'' || s[i] == '"') {
				if (!(i = value(S, i))) { return 0; }
			}
			break;
		case ';':
			for (; (c = s[i]) && c != '\n'; i++)
				;
			if (!c) { return 0; }
			i++;
			break;
		case ')':
		case '\'':
		case '"':
		case '%':
		case '(':
			wprint("Finished parsing keys/values\n");
			return i;
		default:
			whinen(S, i, "Unexpected character '%c'", c);
			return 0;
		}
	}

	return i;
}


size_t escape(State *S, size_t i)
{
	char c;
	if (!S) { eprint("Escape was given a NULL State\n"); return 0; }
	if (S->buffer[i] != '\\') {
		whine0(S, i, "Uh, this should never happen");
		return 0;
	}

	i++;

	switch (S->buffer[i]) {
	case 'n':
	case 'r':
		indent(S, -1);
		dprintf(S->outfd, "<br >");
		indent(S, -1);
		i++;
		for (; (c = S->buffer[i]) && isspace(c); i++);
		break;
	case '\\':
		dprintf(S->outfd, "\\");
		i++;
		break;
	case '"':
		dprintf(S->outfd, "\"");
		i++;
		break;
	case '\'':
		dprintf(S->outfd, "'");
		i++;
		break;
	case 't':
		dprintf(S->outfd, "\t");
		i++;
		break;
	case ' ':
		dprintf(S->outfd, " ");
		i++;
		break;
	default:
		i++;
		break;
	}

	return i;
}


size_t content(State *S, size_t i, char start)
{
	char *s = NULL;
	char c;
	if (!S) { eprint("content was given a null State?\n"); return 0; }

	s = S->buffer;
	i++;

	while ((c = s[i])) {
		switch (c) {
		case '\0':
			whine0(S, i, "Unexpected EOF");
			break;

		case '\\':
			i = escape(S, i);
			break;

		case '&':
			dprintf(S->outfd, "&amp;");
			i++;
			break;

		case '<':
			dprintf(S->outfd, "&lt;");
			i++;
			break;

		case '>':
			dprintf(S->outfd, "&gt;");
			i++;
			break;

		case '\n':
			i++;
			indent(S, -1);
			/* Ignore foreign leading whitespace from the line */
			for (; (c = S->buffer[i]) && isspace(c); i++);
			break;

		case '\'':
		case '"':
			if (c == start) { return i; }
		default:
			dprintf(S->outfd, "%c", c);
			i++;
			break;
		}
	}

	return i;
}


size_t mode(State *S, size_t i, int *mode)
{
	char *s = NULL;

	const char inlin[] = "inline";
	const char compact[] = "compact";
	const char nonew[] = "nonewline";
	const char noind[] = "noindent";
	const size_t inline_size = sizeof inlin - 1;
	const size_t compact_size = sizeof compact - 1;
	const size_t nonew_size = sizeof nonew - 1;
	const size_t noind_size = sizeof noind - 1;

	if (!S) { eprint("mode was given a null State?\n"); return 0; }
	if (!mode) { eprint("mode was given a null Mode\n"); return 0; }

	s = S->buffer;
	*mode = 0;

	/* skip whitespace */
	for (; s[i] && isspace(s[i]); i++)
		;
	
	if (s[i] != '!' && s[i] != '+') {
		return i;
	}
	
	while (s[i] == '!' || s[i] == '+') {
		i++;
		if ((strncmp(s+i, inlin, inline_size)) == 0) {
			*mode |= MODE_INLINE;
			i += inline_size;
		}
		else if ((strncmp(s+i, compact, compact_size)) == 0) {
			*mode |= MODE_COMPACT;
			i += compact_size;
		}
		else if ((strncmp(s+i, nonew, nonew_size)) == 0) {
			*mode |= MODE_NONEWLINE;
			/* i++; TODO: Determine if this breaks anything */
			i += nonew_size;
		}
		else if ((strncmp(s+i, noind, noind_size)) == 0) {
			*mode |= MODE_NOINDENT;
			i += noind_size;
		}
		else {
			whine0(S, i, "Invalid mode");
			return 0;
		}
	}

	return i;
}


int parse(State *S)
{
	size_t i = 0;
	size_t j = 0;
	size_t k = 0;
	size_t open_braces = 0;
	int subnode = 0;
	int m = 0;
	char *s = S->buffer;
	char *d = NULL;
	char c;

	while ((c = s[i]))
	{
		switch (s[i]) {
		case ' ':
		case '\t':
		case '\n':
			i++; break;
		case ';':
			i = skip_line(S, i);
			break;

		case '%':
			i = directive(S, ++i);
			if (!i) { return 0; }
			break;

		case '(':
			i++;
			open_braces++;
			for (j = 1; (c = S->buffer[i+j]) && !isspace(c) && c != ')'; j++)
				;
			if (!c) { whine0(S, i, "Unexpected EOF"); return 0; }

			/* does the parent tag have subnodes? */
			tag.stack[tag.current-1].subnode = 1;
			indent(S, -1);

			if (!pushtag(s, i, j)) { return 0; }
			dprintf(S->outfd, "<%.*s", (int)j, s+i);
			wprint("Found tag '%.*s'\n", (int)j, s+i);
			S->depth++;

			wprint("Started parsing for modes for tag %.*s\n", (int)j, s+i);
			i = mode(S, i+j, &m);
			if (!i) { return 0; }
			if (m) { pushmode(m); }

			wprint("Started parsing keys/values\n");
			i = keys_values(S, i);
			if (!i) { return 0; }
			if (!(m & MODE_INLINE)) {
				dprintf(S->outfd, ">");
			}
			break;

		case '"':
		case '\'':
			if (!S->depth) { whinen(S, i, "Unexpected %c", c); return 0; }
			if (tag.stack[tag.current-1].subnode) {
				indent(S, -1);
			}
			wprint("Found content at line %lu\n", get_line_count(s, i));
			i = content(S, i, s[i]);
			wprint("End of content at line %lu\n", get_line_count(s, i));
			i++;
			break;
		case ')':
			open_braces--;
			if (!S->depth) { whine0(S, i, "Unexpected ')' char"); return 0; }
			S->depth--;

			m = get_current_mode();
			subnode = tag.stack[tag.current-1].subnode;

			if (!poptag(&d, &k, &j)) { return 0; }
			wprint("Found end of tag %.*s\n", (int)j, d+k);

			if (subnode) {
				indent(S, m);
			}
			if ((m & MODE_INLINE)) {
				dprintf(S->outfd, " />");
			}
			else {
				dprintf(S->outfd, "</%.*s>", (int)j, d+k);
			}
			i++;
			break;
		default:
			whinen(S, i, "Unexpected character '%c'", c);
			return 0;
		}
	}
	if (open_braces > 0) {
		whine0(S, i, "Unexpected EOF due to open brace");
		return 0;
	}

	return 1;
}


int parse_file(char *filename, State *inherit)
{
	State S = {0};
	int status = 0;

	if (setup_state(&S, filename, inherit) < 0) { return 0; }
	status = parse(&S);
	clear_state(&S);

	return status;
}


int main(int argc, char **argv)
{
	int status = 1;
	if (!argv[1]) { usage(); return 1; }
	int i = parse_options(argv, 1);
	wprint("Finished parsing options\n");

	if (!tag_init(1024)) { return 1; }

	if (!i) { return 1; }
	if (!argv[i]) { eprint("No input files.\n"); return 1; }

	if (!mkfolder(settings.out_folder)) { return 1; }

	for (; status && argv[i]; i++) {
		wprint("Parsing file %s\n", argv[i]);
		status = parse_file(argv[i], NULL);
	}

	tag_deinit();
	
	if (!status) { return 1; }

	return 0;
}
