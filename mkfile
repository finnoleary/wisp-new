CC=cc
CFLAGS= -std=c99 -pedantic -Wall -Os -D_XOPEN_SOURCE=700 -D_FORTIFY_SOURCE=2 -fstack-protector-strong -fno-omit-frame-pointer # -fsanitize=undefined
CINC=  
CLIB= 

SCAN= `{ (command -v scan-build-7) || echo 'scan-build' }
SCANFLAGS= -enable-checker core -enable-checker security -enable-checker unix

SPARSEFLAGS= -Wsparse-all -gcc-base-dir `gcc --print-file-name=`
CPPCHKFLAGS= --std=posix --std=c99 --platform=unix64 --enable=all --suppress=missingIncludeSystem

FILES= `{ls *.c}

PREFIX=/usr/local
BINDIR=$PREFIX/bin/
MANDIR=$PREFIX/man/man1/

bin/wisp: $FILES
  [ -d bin ] || mkdir bin
  $SCAN $SCANFLAGS $CC $CFLAGS $FILES -o $target $CINC $CLIB

lint:V: $FILES
  [ -d bin ] || mkdir bin
  sparse $SPARSEFLAGS $CINC $FILES
  cppcheck $CPPCHKFLAGS $FILES

clean:
  rm -f *.o
  rm -rf bin
  rm -rf out

install:V: bin/wisp
  install -d $BINDIR
  install -m 755 bin/wisp $BINDIR

uninstall:V:
  rm -f $BINDIR/wisp
